﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Amazon;
using Amazon.Lambda.Model;
using Amazon.Lambda;
using Amazon.Runtime;
using Amazon.CognitoIdentity;
using System;
using UnityEngine.UI;
using System.Text;

public class DynamoDBHandler : MonoBehaviour {
    [SerializeField]
    private AWSController AWS;

    // Use this for initialization
    void Start () {
        AWS = GetComponent<AWSController>();
        if (AWS == null)
        {
            Debug.LogError("DynamoDBHandler: AWS Controller is not in this GameObject");
        }
    }

}
