﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Amazon;
using Amazon.Lambda.Model;
using Amazon.Lambda;
using Amazon.Runtime;
using Amazon.CognitoIdentity;
using System;
using UnityEngine.UI;
using System.Text;

public class AWSController : MonoBehaviour {

    [Header("AWS Configuration")]
    #region private members
    public string IdentityPoolId = "ap-southeast-2:4a88eee5-5602-498f-967e-1a85f69f291e";
    public string CognitoIdentityRegion = RegionEndpoint.APSoutheast2.SystemName;
    public string LambdaRegion = RegionEndpoint.APSoutheast2.SystemName;
    public RegionEndpoint _CognitoIdentityRegion
    {
        get { return RegionEndpoint.GetBySystemName(CognitoIdentityRegion); }
    }
    public RegionEndpoint _LambdaRegion
    {
        get { return RegionEndpoint.GetBySystemName(LambdaRegion); }
    }
    public IAmazonLambda _lambdaClient;
    public AWSCredentials _credentials;

    public AWSCredentials Credentials
    {
        get
        {
            if (_credentials == null)
                _credentials = new CognitoAWSCredentials(IdentityPoolId, _CognitoIdentityRegion);
            return _credentials;
        }
    }
    public IAmazonLambda Client
    {
        get
        {
            if (_lambdaClient == null)
            {
                _lambdaClient = new AmazonLambdaClient(Credentials, _LambdaRegion);
            }
            return _lambdaClient;
        }
    }
    #endregion

    void Awake()
    {
        UnityInitializer.AttachToGameObject(this.gameObject);
        AWSConfigs.HttpClient = AWSConfigs.HttpClientOption.UnityWebRequest;
        
    }

    // Use this for initialization
    void Start()
    {
        OnDataLoading(false);
    }


    public Animator DataLoadingAniamtion;
    public GameObject DataLoadingIcon;

    void OnDataLoading(bool _isLoading)
    {
        DataLoadingIcon.SetActive(_isLoading);
    }

    // =========================================================
    #region AWS Lambda

    /// <summary>
    /// Darkspede_HomeGP_User_SendAuth
    /// </summary>
    /// <param name="_mobile"></param>
    /// <param name="_deviceID"></param>
    private void OnLambda_SendAuthCode(string _mobile, string _deviceID)
    {
        string payload = "{\"deviceID\":\"" + _deviceID + "\",\"mobile\":\"+" + _mobile + "\"}";
        Debug.Log("OnLambda_SendAuthCode: " + payload);

        Client.InvokeAsync(new InvokeRequest()
        {
            FunctionName = "Darkspede_HomeGP_User_SendAuth",
            Payload = payload
        },
        (responseObject) =>
        {
            if (responseObject.Exception == null)
            {
                string result = Encoding.ASCII.GetString(responseObject.Response.Payload.ToArray());
                Debug.Log("OnLambda_SendAuthCode: Result = " + result);
                result = result.Replace('"', ' ').Trim();

                if (result == "NULL")
                {
                    Debug.Log("OnLambda_SendAuthCode: Get Result Fail: ");
  
                }
                else
                {
                    PlayerPrefs.SetString("CurrentUserGuid", result);
                    Debug.Log("OnLambda_SendAuthCode: Get Result Success: " + PlayerPrefs.GetString("CurrentUserGuid"));

                }
            }
            else
            {
                Debug.Log("OnLambda_SendAuthCode: Get Result Fail: ");

            }


        }
        );
    }
    



    private void OnLambda_CheckSoftwareValidation()
    {

    }
    #endregion

    // =========================================================


}
