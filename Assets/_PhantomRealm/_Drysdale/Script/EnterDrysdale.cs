﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnterDrysdale : MonoBehaviour
{
    // Start is called before the first frame update

    public int SceneID;
    void Start()
    {
        SceneManager.LoadScene(SceneID);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
