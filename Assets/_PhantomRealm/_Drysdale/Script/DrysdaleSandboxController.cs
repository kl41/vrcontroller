﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DrysdaleSandboxController : MonoBehaviour
{
    public GameObject MainPlayer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            SceneManager.LoadScene(2);
        }
    }

    void OnLevelWasLoaded()
    {
        MainPlayer = GameObject.FindGameObjectWithTag("MainPlayer");
    }
}
