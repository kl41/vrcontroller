﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class DrysdaleLevelController : MonoBehaviour
{
    public GameObject SceneCamera;
    public GameObject MainPlayer;

    public Transform[] SceneItems;
    public int Index = 0;

    public float speed;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            if(MainPlayer != null)
            {
                Destroy(MainPlayer);
            }

            SceneManager.LoadScene(0);
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            Index++;
            if(Index >= SceneItems.Length)
            {
                Index = 0;
            }
        }

        //SceneCamera.transform.position = Vector3.Lerp(SceneCamera.transform.position, SceneItems[Index].position, Time.deltaTime * speed);
        //SceneCamera.transform.rotation = Quaternion.Lerp(SceneCamera.transform.rotation, SceneItems[Index].rotation, Time.deltaTime * speed);

    }

    void OnLevelWasLoaded()
    {
        //MainPlayer = GameObject.FindGameObjectWithTag("MainPlayer");
    }
}
