﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    public AudioSource AudioSource;
    public AudioClip[] Clips;
    public enum SoundSelector
    {
        none,
        debug,
        onSelect,
    }

    // Start is called before the first frame update
    void Start()
    {
        if (AudioSource != null)
        {
            AudioSource.loop = false;
        }
        else
        {
            Debug.LogError("SoundPlayer: No audio source component selected");
        }

    }


    public void PlayerSound(SoundSelector _selction)
    {
        switch (_selction) {
            case SoundSelector.debug:
                LoadClip(1);
                break;
            case SoundSelector.onSelect:
                LoadClip(2);
                break;
            default:
                LoadClip(0);
                break;
        }

        AudioSource.Play();
    }

    void LoadClip(int _index)
    {
        if(Clips[_index] != null)
        {
            AudioSource.clip = Clips[_index];
        }
    }


}
