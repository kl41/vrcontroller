﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarControllerSelector : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {
        TouchInput();
    }



    private void TouchInput()
    {
        // 右手扳机按键
        if (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger))
        {

        }

        // 右手手部按键
        if (OVRInput.GetDown(OVRInput.Button.SecondaryHandTrigger))
        {

        }

        // 右手A按键
        if (OVRInput.GetDown(OVRInput.Button.One))
        {

        }

        // 右手B按键
        if (OVRInput.GetDown(OVRInput.Button.Two))
        {

        }


        // 左手扳机按键
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
        {

        }

        // 左手手部按键
        if (OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger))
        {

        }


        // 左手X按键
        if (OVRInput.GetDown(OVRInput.Button.Three))
        {

        }

        // 左手Y按键
        if (OVRInput.GetDown(OVRInput.Button.Four))
        {

        }

    }
}
