﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControllerHandler : MonoBehaviour
{
    [Header("Controller Type")]
    public GameObject[] LeftControllers;
    public GameObject[] RightControllers;

    public enum ControllerType
    {
        touch,
        hand,
        game,
        gun,
        sword,
    }

    public ControllerType CurrentControllerType = ControllerType.touch;


    // [0] is printer left hand [1] is printer right hand

    [Header ("Controller Pointers")]
    public GameObject[] Pointers;
    public enum PointerMode {
        none,
        lefthand,
        righthand,
        both
    }

    public PointerMode CurrentPointerMode = PointerMode.righthand;


    // Start is called before the first frame update
    void Start()
    {
        if(LeftControllers.Length != RightControllers.Length)
        {
            Debug.LogError("TouchControllerHandler: Controller objects are not set currect!");
        }

        if(Pointers.Length != 2)
        {
            Debug.LogError("TouchControllerHandler: Pointer objects are not set currect!");
        }

        SetPointerMode(CurrentPointerMode);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void SetPointerMode(PointerMode _mode)
    {
        switch (_mode) {
            case PointerMode.none:
                Pointers[0].SetActive(false);
                Pointers[1].SetActive(false);
                break;
            case PointerMode.lefthand:
                Pointers[0].SetActive(true);
                Pointers[1].SetActive(false);
                break;
            case PointerMode.righthand:
                Pointers[0].SetActive(false);
                Pointers[1].SetActive(true);
                break;
            case PointerMode.both:
                Pointers[0].SetActive(true);
                Pointers[1].SetActive(true);
                break;
        }

        CurrentPointerMode = _mode;

    }



    public void SetControllerType(ControllerType _type)
    {
        switch (_type)
        {
            case ControllerType.touch:
                LeftControllers[0].SetActive(true);
                RightControllers[0].SetActive(true);
                break;
            case ControllerType.hand:
                LeftControllers[1].SetActive(true);
                RightControllers[1].SetActive(true);
                break;
            case ControllerType.game:
                LeftControllers[2].SetActive(true);
                RightControllers[2].SetActive(true);
                break;
            case ControllerType.gun:
                LeftControllers[3].SetActive(true);
                RightControllers[3].SetActive(true);
                break;
            case ControllerType.sword:
                LeftControllers[4].SetActive(true);
                RightControllers[4].SetActive(true);
                break;
        }

        CurrentControllerType = _type;
    }


    public Transform GetPointerTransform(bool _isLeft)
    {
        if (_isLeft)
        {
            return Pointers[0].transform;
        }
        else
        {
            return Pointers[1].transform;
        }
    }
}
