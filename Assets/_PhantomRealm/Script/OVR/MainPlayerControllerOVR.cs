﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class MainPlayerControllerOVR : MonoBehaviour
{
    [Header("Components")]
    public SoundPlayer SoundPlayer;
    public TouchControllerHandler TouchControllerHandler;

    [Header("Components")]
    public GameObject HUD;
    public Transform HeadPosition;
    public Transform FloorMarker;


    [Header("Trigger Selection")]
    public Transform InitialPosition;
    public LayerMask SelectionLayer;
    public LayerMask FloorLayer;
    public int SelectionDistance;


    private RaycastHit hits;
    private float StartButtonCounter;
    // Start is called before the first frame update
    void Start()
    {
        //DontDestroyOnLoad(this);
        HUD = GameObject.FindGameObjectWithTag("HUD");
        //transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        InitialPosition = TouchControllerHandler.GetPointerTransform(false);
        HUD.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        TouchInput();

        if (Input.GetKeyDown(KeyCode.T))
        {
            if (Physics.Raycast(InitialPosition.position, InitialPosition.forward, out hits, SelectionDistance, SelectionLayer))
            {
                if (hits.transform.gameObject.tag == "Floor")
                {
                    Vector3 newPosition = hits.point - HeadPosition.localPosition;
                    transform.position = new Vector3(newPosition.x, transform.position.y, newPosition.z);
                }
                else
                {
                    hits.transform.gameObject.GetComponent<ObjectSelectEnable>().OnObjectSelect();
                }
            }
        }
    }


    private void TouchInput()
    {
        // right hand trigger
        if (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger))
        {
            if (TouchControllerHandler.CurrentPointerMode != TouchControllerHandler.PointerMode.righthand)
            {
                TouchControllerHandler.SetPointerMode(TouchControllerHandler.PointerMode.righthand);
                InitialPosition = TouchControllerHandler.GetPointerTransform(false);
            }
            else
            {
                if (Physics.Raycast(InitialPosition.position, InitialPosition.forward, out hits, SelectionDistance, SelectionLayer))
                {
                    Debug.Log("MainPlayerControllerOVR： Ray hitting " + hits.transform.gameObject.name);
                    if (hits.transform.gameObject.tag == "Floor")
                    {
                        Vector3 newPosition = hits.point - HeadPosition.localPosition;
                        transform.position = new Vector3(newPosition.x, hits.point.y, newPosition.z);
                    }
                    else
                    {
                        hits.transform.gameObject.GetComponent<ObjectSelectEnable>().OnObjectSelect();
                    }
                }
            }
        }

        // 右手手部按键
        if (OVRInput.GetDown(OVRInput.Button.SecondaryHandTrigger))
        {
            //OVRInput.SetControllerVibration(0.1f, 0.1f, OVRInput.Controller.RTouch);
        }

        // A
        if (OVRInput.GetDown(OVRInput.Button.One))
        {

        }

        // B
        if (OVRInput.GetDown(OVRInput.Button.Two))
        {

        }


        // 左手扳机按键
        if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger))
        {
            if (TouchControllerHandler.CurrentPointerMode != TouchControllerHandler.PointerMode.lefthand)
            {
                TouchControllerHandler.SetPointerMode(TouchControllerHandler.PointerMode.lefthand);
                InitialPosition = TouchControllerHandler.GetPointerTransform(true);
            }
            else
            {
                if (Physics.Raycast(InitialPosition.position, InitialPosition.forward, out hits, SelectionDistance, SelectionLayer))
                {
                    Debug.Log("MainPlayerControllerOVR： Ray hitting " + hits.transform.gameObject.name);
                    if (hits.transform.gameObject.tag == "Floor")
                    {
                        Vector3 newPosition = hits.point - HeadPosition.localPosition;
                        transform.position = new Vector3(newPosition.x, hits.point.y, newPosition.z);
                    }
                    else
                    {
                        hits.transform.gameObject.GetComponent<ObjectSelectEnable>().OnObjectSelect();
                    }
                }
            }
        }

        // 左手手部按键
        if (OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger))
        {

        }


        // X
        if (OVRInput.GetDown(OVRInput.Button.Three))
        {

        }

        // Y
        if (OVRInput.GetDown(OVRInput.Button.Four))
        {

        }

        if (OVRInput.Get(OVRInput.Button.Four))
        {
            if (StartButtonCounter > 2f && StartButtonCounter < 3f)
            {
                StartButtonCounter = 3f;
                SceneManager.LoadScene(0); // 
            }
            else
            {
                StartButtonCounter = StartButtonCounter + Time.deltaTime;
            }

            if (OVRInput.GetUp(OVRInput.Button.Four))
            {
                StartButtonCounter = 0;
            }

        }

        //  START 
        if (OVRInput.Get(OVRInput.Button.Start))
        {

            if(StartButtonCounter > 2f && StartButtonCounter <3f)
            {
                StartButtonCounter = 3f;
                HUD.SetActive(HUD.activeSelf ? false : true);
                SoundPlayer.PlayerSound(SoundPlayer.SoundSelector.debug);
            }
            else
            {
                StartButtonCounter = StartButtonCounter + Time.deltaTime;
            }
        }

        if (OVRInput.GetUp(OVRInput.Button.Start))
        {
            StartButtonCounter = 0;
        }

    }
}
