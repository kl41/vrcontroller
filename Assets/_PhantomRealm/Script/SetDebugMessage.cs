﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetDebugMessage : MonoBehaviour
{
    public static Text DebugMessageText;

    public Text debugMessageText;


    void Start()
    {
        DebugMessageText = debugMessageText;
    }


    public static void SetMessage(string _message)
    {
        DebugMessageText.text = _message;
    }


    void OnDisable()
    {
        DebugMessageText = null;
    }


}
