﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextBlink : MonoBehaviour
{
    public TextMesh TextMesh;

    public Color[] Colors;
    public float Freqency = 1f;
    public float Speed = 1f;
    public bool IsEnabled = true;


    public int Direct = 0;
    public float Counter;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (IsEnabled)
        {
            if(Counter > Freqency)
            {
                Direct++;
                Counter = 0; 

                if(Direct >= Colors.Length)
                {
                    Direct = 0;
                }
            }else
            {
                Counter += Time.deltaTime;
            }



            TextMesh.color = Color.Lerp(TextMesh.color, Colors[Direct], Time.deltaTime * Speed);
        }
    }
}
