﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSelected : MonoBehaviour
{

    public GameObject Selected;
    public GameObject Target;

    public bool IsSeleted;
    

    // Start is called before the first frame update
    void Start()
    {

        // should a gameobject attached this scirpt, the tag of 
        gameObject.tag = "Selection";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    

    private void OnTriggerEnter(Collider collision)
    {
        Selected.SetActive(true);
    }

    private void OnTriggerStay(Collider other)
    {
        if (!Selected.activeSelf)
        {
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Selected.SetActive(false);
    }
}
