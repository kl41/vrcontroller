﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSelectEnable : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject EnableObjectOnDetect;
    public GameObject EnableObjectOnSelect;
    void Start()
    {
        EnableObjectOnDetect.SetActive(false);

        if(EnableObjectOnSelect != null)
        {
            EnableObjectOnSelect.SetActive(false);
        }

    }

    public void OnObjectDetected()
    {
        if (!EnableObjectOnDetect.activeSelf)
        {
            EnableObjectOnDetect.SetActive(true);
        }
    }

    public void Deactivate()
    {
        EnableObjectOnDetect.SetActive(false);
    }


    public void OnObjectSelect()
    {
        if (EnableObjectOnSelect != null)
        {
            EnableObjectOnSelect.SetActive(true);
        }
    }


}
