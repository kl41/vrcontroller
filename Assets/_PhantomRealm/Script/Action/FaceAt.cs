﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceAt : MonoBehaviour
{
    public Transform Position;
    public bool IsFacingCamare = true;
    public bool IsFlip = true;


    // Start is called before the first frame update
    void Start()
    {
        if (IsFlip)
        {
            transform.localScale = new Vector3(transform.localScale.x * -1f, transform.localScale.y, transform.localScale.z);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Position == null)
        {
            Position = Camera.main.transform;
            if (IsFlip)
            {
                Position.localScale = new Vector3(-Position.localScale.x, Position.localScale.y, Position.localScale.z);
            }
        }
        else
        {
            transform.LookAt(Position);
        }

    }
}
