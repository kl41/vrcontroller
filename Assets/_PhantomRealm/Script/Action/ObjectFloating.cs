﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectFloating : MonoBehaviour
{
    // Start is called before the first frame update
    public bool IsActive = true;
    public bool IsRotate = true;

    public float InitialHeight;
    public float Frequency;
    public float Range;
    public float DegreesPerSecond;

    private Vector3 defaultPosition;
    private Vector3 Target;
    void Start()
    {
        if (InitialHeight != 0) {
            transform.position = new Vector3(transform.position.x, InitialHeight, transform.position.z);
        }

        defaultPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (IsActive)
        {
            if (IsRotate)
            {
                transform.Rotate(new Vector3(0f, Time.deltaTime * DegreesPerSecond, 0f), Space.World);
            }
            Target = defaultPosition;
            Target.y += Mathf.Sin(Time.fixedTime * Mathf.PI * Frequency) * Range;
            transform.position = Target;
        }
    }




}
