﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDetector : MonoBehaviour
{
    public bool IsDetectionOn = true;

    public Transform InitialPosition;
    public Transform DetectedObject;
    public Transform LastDetectedObject;

    public LayerMask DetectionLayer;
    public int DetectionDistance;


    public LayerMask FloorLayer;

    public FloorMarkerController FloorMarker;

    private RaycastHit hits;
    private Vector3 HitPosition;
    private Vector3 HitFaceNormal;
    private ObjectSelected HitObject;

    // Start is called before the first frame update
    void Start()
    {
        if (InitialPosition == null)
        {
            InitialPosition = transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (IsDetectionOn)
        {
            OnDetectionCasting();
            //Debug.DrawRay(transform.position, transform.forward, Color.green, 1f);
        }
    }



    private void OnDetectionCasting()
    {
        if(Physics.Raycast(InitialPosition.position, InitialPosition.forward, out hits, DetectionDistance, DetectionLayer))
        {
            HitPosition = hits.point;
            HitFaceNormal = hits.normal;
            Debug.Log("OnDetectionCasting: ");
            if (hits.transform.gameObject.tag == "Floor")
            {
                if (FloorMarker != null)
                {
                    FloorMarker.SetMarker(true);
                    FloorMarker.transform.position = hits.point;
                }
            }
            else
            {
                if (FloorMarker != null)
                {
                    FloorMarker.SetMarker(false);
                }

                if (LastDetectedObject != null)
                {
                    LastDetectedObject.gameObject.GetComponent<ObjectSelectEnable>().Deactivate();
                }

                hits.transform.gameObject.GetComponent<ObjectSelectEnable>().OnObjectDetected();
                LastDetectedObject = hits.transform;
            }
        }
        else
        {
            if (LastDetectedObject != null)
            {
                LastDetectedObject.gameObject.GetComponent<ObjectSelectEnable>().Deactivate();
                LastDetectedObject = null;
            }

            if (LastDetectedObject != null)
            {
                FloorMarker.SetMarker(false);
            }

        }
    }
}
