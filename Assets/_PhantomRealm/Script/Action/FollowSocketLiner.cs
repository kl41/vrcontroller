﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowSocketLiner : MonoBehaviour
{
    public Transform Socket;
    public bool IsEnabled;
    public float Speed = 1f;
    public float HeightLimit;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (IsEnabled)
        {
            transform.position = Vector3.Lerp(transform.position, Socket.position, Time.deltaTime * Speed);
            if(transform.position.y < HeightLimit)
            {
                transform.position = new Vector3(transform.position.x, HeightLimit, transform.position.z);
            }
        }
    }



    public void SetEnable(bool _enable)
    {
        IsEnabled = _enable;
    }
}
