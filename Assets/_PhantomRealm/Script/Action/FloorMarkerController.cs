﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorMarkerController : MonoBehaviour
{
    public GameObject Maker;

    public void SetMarker(bool _isON)
    {
        Maker.SetActive(_isON);
    }
}
