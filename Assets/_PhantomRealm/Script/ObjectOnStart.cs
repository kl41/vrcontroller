﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectOnStart : MonoBehaviour
{

    public enum OnstartAction {
        Disable,
        Enable,
        Destroy,
        AddtoParent,
    }
    public OnstartAction Action;
    public Transform ParentObject;

    // Start is called before the first frame update
    void Start()
    {
        switch (Action)
        {
            case OnstartAction.Disable:
                gameObject.SetActive(false);
                break;
            case OnstartAction.Enable:
                gameObject.SetActive(true);
                break;
            case OnstartAction.Destroy:
                Destroy(gameObject);
                break;
            case OnstartAction.AddtoParent:
                if(ParentObject != null)
                {
                    transform.parent = ParentObject;
                }
                break;
            default:
                Destroy(gameObject);
                break;
        }
    }
}
