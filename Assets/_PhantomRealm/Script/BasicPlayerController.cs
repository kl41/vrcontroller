﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BasicPlayerController : MonoBehaviour
{
    public GameObject TestingCubePrefabs;
    public Transform TestingCubeInitalSpot;

    public TextMesh CubeCounter;

    public GameObject[] DisplayItems;


    private List<GameObject> CubeList = new List<GameObject>();
    private int Item_Index;

    // Start is called before the first frame update
    void Start()
    {
        CubeCounter.text = "Cube: " + CubeList.Count.ToString();
        /*
        if (DisplayItems.Length > 0)
        {
            foreach(GameObject item in DisplayItems)
            {
                item.SetActive(false);
            }
        }
        */
        Item_Index = 0;
        //DisplayItems[Item_Index].SetActive(true);

    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.One) || Input.GetKeyDown(KeyCode.Alpha1))
        {
            GameObject cube = Instantiate(TestingCubePrefabs, TestingCubeInitalSpot);
            CubeList.Add(cube);
            CubeCounter.text = "Cube: "+ CubeList.Count.ToString();
        }

   
        if (OVRInput.GetDown(OVRInput.Button.Two) || Input.GetKeyDown(KeyCode.Alpha2))
        {
            foreach(GameObject cube in CubeList)
            {
                Destroy(cube);
            }

            CubeList.Clear();
            CubeCounter.text = "Cube: " + CubeList.Count.ToString();
        }

        if (OVRInput.GetDown(OVRInput.Button.Three))
        {
            OVRManager.display.RecenterPose();
        }


        if (OVRInput.GetDown(OVRInput.Button.Four))
        {
            /*
            DisplayItems[Item_Index].SetActive(false);
            Item_Index++;
            if(Item_Index >= DisplayItems.Length)
            {
                Item_Index = 0;
            }
            DisplayItems[Item_Index].SetActive(true);
            */

        }


    }



    void ControllerInput()
    {

    }
}
