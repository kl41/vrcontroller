﻿
using UnityEngine;
using UnityEngine.UI;

public class DebugData : MonoBehaviour
{
    public static int OVR_FPS;
    public static int SetPassCall;
    public static int Tris;

    public Text UIText;
    public TextMesh UITextMesh;

    float updateInterval = 0.2f;
    float fps, ms;
    int m_frameCounter = 0;
    float m_timeCounter = 0.0f;
    float m_lastFramerate = 0.0f;
    public float m_refreshTime = 0.5f;

    void Awake()
    {
        Application.targetFrameRate = 75; // for oculus quest
    }

    void Start()
    {

    }

    void Update()
    {
        FPS_Realtime();
        OVR_FPS = (int)fps;
    }

    private void FPS_Realtime()
    {
        if (m_timeCounter < m_refreshTime)
        {
            m_timeCounter += Time.deltaTime;
            m_frameCounter++;
        }
        else
        {
            //This code will break if you set your m_refreshTime to 0, which makes no sense.
            m_lastFramerate = (float)m_frameCounter / m_timeCounter;
            m_frameCounter = 0;
            m_timeCounter = 0.0f;
            fps = m_lastFramerate;

            if (UIText != null)
            {
                UIText.text = GetFPSString();
            }

            if (UITextMesh != null)
            {
                UITextMesh.text = GetFPSString();
            }
        }

    }

    public float GetNow()
    {
        return fps;
    }
    public string GetFPSString()
    {
        int newFPS = (int)fps;
        return newFPS.ToString();
    }




}
