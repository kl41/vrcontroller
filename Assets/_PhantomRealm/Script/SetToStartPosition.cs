﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetToStartPosition : MonoBehaviour
{
    public Vector3 StartPosition = new Vector3(0, 0, 0);
    public Vector3 StartRotation = new Vector3(0, 0, 0);
    public bool ifSetPosition = true;
    public bool ifSetRotation = false;


    void Awake()
    {
        if (ifSetPosition)
        {
            transform.position = StartPosition;
        }

        if (ifSetRotation)
        {
            transform.rotation = Quaternion.identity;
        }
    }

}
