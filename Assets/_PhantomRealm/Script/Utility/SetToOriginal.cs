﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetToOriginal : MonoBehaviour
{
    public Vector3 Original;

    public bool ResetOrientation = true;
    public bool SetToLocal = false;

    // Start is called before the first frame update
    void Start()
    {
        if (SetToLocal)
        {
            this.transform.position = Original;
            this.transform.rotation = Quaternion.identity;
        }
        else
        {
            this.transform.localPosition = Original;
            this.transform.localRotation = Quaternion.identity;
        }
    }

}
