﻿//=============================================================================================
// Phantom Realm: Titan Descent
// ShowFPS.cs
// Created: 2017/04/19
// Author: Yifeng Chen
// Modifier: Yifeng Chen, add more here..
//=============================================================================================
using UnityEngine;
using UnityEngine.UI;
using System.Collections;


    public class ShowFPS : MonoBehaviour
    {
        private float updateInterval = 0.2f;
        private float lastInterval;
        private float frames = 0f;
        private float fps, ms;

        public Text UIText;
        public TextMesh UITextMesh;


        void Awake()
        {
            Application.targetFrameRate = -1; 
        }


        void Start()
        {
            lastInterval = Time.realtimeSinceStartup;
            frames = 0;
        }

         void Update()
        {
            FPS_Realtime();
        }


        private void FPS_Overall()
        {
            ++frames;
            float timeNow = Time.realtimeSinceStartup;
            if (timeNow > lastInterval + updateInterval)
            {
                fps = frames / (timeNow - lastInterval);
                //ms = 1000.0f / Mathf.Max(fps, 0.00001f);
                frames = 0;
                lastInterval = timeNow;
            }
        }

        int m_frameCounter = 0;
        float m_timeCounter = 0.0f;
        float m_lastFramerate = 0.0f;
        public float m_refreshTime = 0.5f;

        private void FPS_Realtime()
        {
            if (m_timeCounter < m_refreshTime)
            {
                m_timeCounter += Time.deltaTime;
                m_frameCounter++;
            }
            else
            {
                //This code will break if you set your m_refreshTime to 0, which makes no sense.
                m_lastFramerate = (float)m_frameCounter / m_timeCounter;
                m_frameCounter = 0;
                m_timeCounter = 0.0f;
                fps = m_lastFramerate;

                if(UIText != null)
                {
                    UIText.text = GetFPSString();
                }

                if (UITextMesh != null)
                {
                    UITextMesh.text = GetFPSString();
                }
        }

        }

        void OnDisable()
        {

        }

        public float GetNow()
        {
            return fps;
        }

    public string GetFPSString()
    {
        int newFPS = (int)fps;
        return newFPS.ToString();
    }
}



