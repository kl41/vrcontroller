﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;

public class Utility
{
    // USUALLY the local network ID assigned by router
    private static string LOCAL_IP_PREFIX = "192";

    public static float CalculateAngle(Vector3 from, Vector3 to)
    {
        return Quaternion.FromToRotation(Vector3.up, to - from).eulerAngles.z;
    }

    public static float Get1Dfloat(float _input)
    {
        return (int)(Mathf.Round(_input * 10)) / 10f;
    }

    public static float Get2Dfloat(float _input)
    {
        return (int)(Mathf.Round(_input * 100)) / 100f;
    }

    public static float Get3Dfloat(float _input)
    {
        return (int)(Mathf.Round(_input * 1000)) / 1000f;
    }

    public static float Get4Dfloat(float _input)
    {
        return (int)(Mathf.Round(_input * 10000)) / 10000f;
    }

    public static Vector3 GetCentroidPoint(List<Vector3> _points, bool _if3Dpoint = false)
    {
        Vector3 center = new Vector3(0f, 0f, 0f);
        float x = 0f, y = 0f, z = 0f;
        int count = 0;
        foreach (Vector3 point in _points)
        {
            x += point.x;
            y += point.y;
            z += point.z;
            count++;
        }
        center.x = x / count;
        center.z = z / count;

        if (_if3Dpoint)
        {
            center.y = y / count;
        }

        return center;
    }
    /// <summary>
    /// get the degree shifted using value range from -180 to 180
    /// </summary>
    /// <returns></returns>
    public static float GetDegreeShift(float _dLast, float _dThis)
    {
        float diff = Mathf.Abs(_dThis - _dLast);
        if (diff > 180)
        {
            diff = 360 - diff;
        }
        return diff;
    }

    /// <summary>
    /// Check if value changed are within tolerence
    /// </summary>
    /// <param name="_inputA"></param>
    /// <param name="_inputB"></param>
    /// <param name="_t"></param>
    /// <returns></returns>
    public static bool GetDiffTolerance(float _inputA, float _inputB, float _t)
    {
        if (GetPointDistance(_inputA, _inputB) < _t)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static float GetPointDistance(float _a, float _b)
    {
        return Mathf.Abs(_a - _b);
    }
    /// <summary>
    /// Get 3D distance between two object
    /// </summary>
    public static float GetDistance(Transform _from, Transform _to)
    {
        return Vector3.Distance(_from.position, _to.position);
    }

    /// <summary>
    /// Get 3D distance between two object
    /// </summary>
    public static float GetDistance(Vector3 _from, Vector3 _to)
    {
        return Vector3.Distance(_from, _to);
    }

    public static float GetDistanceOnPlane(Vector3 _from, Vector3 _to)
    {

        return Vector3.Distance(new Vector3(_from.x, 0, _from.z), (new Vector3(_to.x, 0, _to.z)));
    }

    public static Vector3 GetRandonVector3()
    {
        return new Vector3(Random.Range(0, 360f), Random.Range(0, 360f), Random.Range(0, 360f));
    }

    public static Vector3 GetRandonVector3WithRange(float _minX, float _maxX, float _minY, float _maxY, float _minZ, float _maxZ)
    {
        return new Vector3(Random.Range(_minX, _maxX), Random.Range(_minY, _maxY), Random.Range(_minZ, _maxZ));
    }

    public static Vector3 GetDirectionBetweenTwoVector(Vector3 _impactPoint, Vector3 _targetPoint)
    {
        return _targetPoint - _impactPoint;
    }

    public static string GetLocalIP()
    {
        string ip_address = "";

        try
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    // Sometimes this give you two address
                    // The first one is yuor local IP usually
                    ip_address = ip.ToString();
                    if (ip_address.StartsWith(LOCAL_IP_PREFIX))
                    {
                        break;
                    }
                }
            }
        }
        catch (System.Exception e)
        {
            return "";
        }


        return ip_address;
    }

    public static Vector3 GetPitchYawRollRad(Quaternion rotation)
    {
        float roll = Mathf.Atan2(2 * rotation.y * rotation.w - 2 * rotation.x * rotation.z, 1 - 2 * rotation.y * rotation.y - 2 * rotation.z * rotation.z);
        float pitch = Mathf.Atan2(2 * rotation.x * rotation.w - 2 * rotation.y * rotation.z, 1 - 2 * rotation.x * rotation.x - 2 * rotation.z * rotation.z);
        float yaw = Mathf.Asin(2 * rotation.x * rotation.y + 2 * rotation.z * rotation.w);

        return new Vector3(pitch, yaw, roll);
    }

    public static Vector3 GetPitchYawRollDeg(Quaternion rotation)
    {
        Vector3 radResult = GetPitchYawRollRad(rotation);
        return new Vector3(radResult.x * Mathf.Rad2Deg, radResult.y * Mathf.Rad2Deg, radResult.z * Mathf.Rad2Deg);
    }

    /// <summary>
    /// Ranage normal to -180 t0 180
    /// </summary>
    /// <param name="_input"></param>
    /// <returns></returns>
    public static float SetDegreeTo180Range(float _input)
    {
        if (_input > 180)
        {
            return _input - 360f;
        }
        else if (_input < -180)
        {
            return _input + 360f;
        }
        else
        {
            return _input;
        }
    }

    /// <summary>
    /// Ranage normal to 0 t0 360
    /// </summary>
    /// <param name="_input"></param>
    /// <returns></returns>
    public static float SetDegreeTo360Range(float _input)
    {
        if (_input > 360)
        {
            return _input = _input - 360f;
        }
        else if (_input < 0)
        {
            return _input = _input + 360f;
        }
        else
        {
            return _input;
        }
    }

    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [System.Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }

    public static bool LineLineIntersection(out Vector3 intersection, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2)
    {

        Vector3 lineVec3 = linePoint2 - linePoint1;
        Vector3 crossVec1and2 = Vector3.Cross(lineVec1, lineVec2);
        Vector3 crossVec3and2 = Vector3.Cross(lineVec3, lineVec2);

        float planarFactor = Vector3.Dot(lineVec3, crossVec1and2);

        //is coplanar, and not parrallel
        if (Mathf.Abs(planarFactor) < 0.0001f && crossVec1and2.sqrMagnitude > 0.0001f)
        {
            float s = Vector3.Dot(crossVec3and2, crossVec1and2) / crossVec1and2.sqrMagnitude;
            intersection = linePoint1 + (lineVec1 * s);
            return true;
        }
        else
        {
            intersection = Vector3.zero;
            return false;
        }
    }

    public static Vector3 GetLine2DVectorByDegree(float degree)
    {
        Vector3 line = new Vector3(0, 0, 0);
        line.x = Mathf.Sin(Mathf.Deg2Rad * degree);
        line.z = Mathf.Cos(Mathf.Deg2Rad * degree);
        return line;
    }

    public static Vector3 GetLine2DVectorByDegreeXY(float degree)
    {
        Vector3 line = new Vector3(0, 0, 0);
        line.x = Mathf.Sin(Mathf.Deg2Rad * degree);
        line.y = Mathf.Cos(Mathf.Deg2Rad * degree);
        return line;
    }

    public static bool CheckIfPointIsFront(Transform _this, Vector3 _target)
    {
        if (Vector3.Dot(_target - _this.position, _this.forward) < 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
