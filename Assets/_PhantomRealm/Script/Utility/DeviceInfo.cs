﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceInfo : MonoBehaviour
{
    public TextMesh Text_DeviceInfo;
    public TextMesh Text_DebugMessage;
    public Transform HeadPosition;
    public Transform AnchorPosition;
    // Start is called before the first frame update

    private float battery;
    private int cpu;
    private int gpu;

    private string device_info;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        battery = OVRManager.batteryLevel * 100;
        cpu = OVRManager.cpuLevel;
        gpu = OVRManager.gpuLevel;

        device_info = string.Format("BAT[{0:0.0}%]\nCPU[L{1}]\nGPU[L{2}] ", battery, cpu, gpu);
        Text_DeviceInfo.text = device_info;
        Text_DebugMessage.text = string.Format("({0})\n({1})", HeadPosition.localPosition, AnchorPosition);
    }
}
